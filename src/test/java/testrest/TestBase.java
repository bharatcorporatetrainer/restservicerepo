package testrest;
import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;



import org.junit.Assert;

public class TestBase {
	public static RequestSpecification REQUEST;
	public static Faker FAKER = new Faker();

	// public TestBase() {
	
	 @Test
	  public void Resttest() throws Exception {


		REQUEST = RestAssured.given().contentType(ContentType.JSON);

		RequestSpecification httpRequest = RestAssured.given();

		Response response = httpRequest.get("http://dummy.restapiexample.com/api/v1/employees");

		System.out.println("Response Body is =>  " + response.asString());
		int statusCode = response.getStatusCode();

		// Assert that correct status code is returned.
		Assert.assertEquals(statusCode, 200);

	}
}
